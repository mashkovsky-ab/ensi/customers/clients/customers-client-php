# # CustomerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор покупателя | [optional] 
**avatar** | [**\Ensi\CustomersClient\Dto\File**](File.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания пользователя | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления пользователя | [optional] 
**full_name** | **string** | Полное ФИО | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


