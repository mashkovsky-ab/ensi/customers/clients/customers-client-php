# # CustomerAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор адреса | [optional] 
**customer_id** | **int** | Идентификатор клиента | [optional] 
**default** | **bool** | Является адресом по-умолчанию | [optional] 
**address** | [**\Ensi\CustomersClient\Dto\CustomerAddressFillablePropertiesAddress**](CustomerAddressFillablePropertiesAddress.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


