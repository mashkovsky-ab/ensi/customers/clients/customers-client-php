# # MessageForChangeEmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | ID пользователя | [optional] 
**token** | **string** | Токен для изменения email | [optional] 
**new_email** | **string** | Новая почта | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


