# # SearchCustomersFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор покупателя | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**status** | **int** | Статус пользователя из Statuses | [optional] 
**active** | **bool** | Активность пользователя | [optional] 
**email** | **string** | Email | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


