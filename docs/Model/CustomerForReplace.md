# # CustomerForReplace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Идентификатор пользователя | [optional] 
**status_id** | **int** | Идентификатор статуса пользователя | [optional] 
**manager_id** | **int** | Идентификатор менеджера | [optional] 
**yandex_metric_id** | **string** | Идентификатор пользователя в YandexMetric | [optional] 
**google_analytics_id** | **string** | Идентификатор пользователя в GoogleAnalytics | [optional] 
**active** | **bool** | Активность пользователя | [optional] 
**email** | **string** | Email | [optional] 
**phone** | **string** | Телефон | [optional] 
**last_name** | **string** | Фамилия | [optional] 
**first_name** | **string** | Имя | [optional] 
**middle_name** | **string** | Отчество | [optional] 
**gender** | **int** | Пол из CustomerGenderEnum | [optional] 
**create_by_admin** | **bool** | Пользователь создан администратором | [optional] 
**city** | **string** | Город клиента | [optional] 
**birthday** | [**\DateTime**](\DateTime.md) | День рождения | [optional] 
**last_visit_date** | [**\DateTime**](\DateTime.md) | Дата последней авторизации | [optional] 
**comment_status** | **string** | Коментарий к статусу клиента | [optional] 
**timezone** | **string** | Временная зона | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


