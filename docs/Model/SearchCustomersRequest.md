# # SearchCustomersRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**\Ensi\CustomersClient\Dto\SearchCustomersFilter**](SearchCustomersFilter.md) |  | [optional] 
**include** | **string[]** |  | [optional] 
**pagination** | [**\Ensi\CustomersClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


