# # SearchCustomerStatusesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CustomersClient\Dto\CustomerStatuses[]**](CustomerStatuses.md) |  | 
**meta** | [**\Ensi\CustomersClient\Dto\SearchCustomersResponseMeta**](SearchCustomersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


