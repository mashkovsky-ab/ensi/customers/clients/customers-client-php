# Ensi\CustomersClient\StatusesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomerStatus**](StatusesApi.md#createCustomerStatus) | **POST** /customers/statuses | Создание объекта типа Status
[**deleteCustomerStatus**](StatusesApi.md#deleteCustomerStatus) | **DELETE** /customers/statuses/{id} | Удаление объекта типа Status
[**getCustomerStatus**](StatusesApi.md#getCustomerStatus) | **GET** /customers/statuses/{id} | Получение объекта типа Status
[**replaceCustomerStatuses**](StatusesApi.md#replaceCustomerStatuses) | **PATCH** /customers/statuses/{id} | Замена объекта типа Status
[**searchCustomerStatuses**](StatusesApi.md#searchCustomerStatuses) | **POST** /customers/statuses:search | Поиск объектов типа Status



## createCustomerStatus

> \Ensi\CustomersClient\Dto\CustomerStatusResponse createCustomerStatus($customer_status_for_create_or_replace)

Создание объекта типа Status

Создание объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_status_for_create_or_replace = new \Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace(); // \Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace | 

try {
    $result = $apiInstance->createCustomerStatus($customer_status_for_create_or_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->createCustomerStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_status_for_create_or_replace** | [**\Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace**](../Model/CustomerStatusForCreateOrReplace.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerStatusResponse**](../Model/CustomerStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerStatus

> \Ensi\CustomersClient\Dto\EmptyDataResponse deleteCustomerStatus($id)

Удаление объекта типа Status

Удаление объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCustomerStatus($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->deleteCustomerStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCustomerStatus

> \Ensi\CustomersClient\Dto\CustomerStatusResponse getCustomerStatus($id, $include)

Получение объекта типа Status

Получение объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCustomerStatus($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->getCustomerStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CustomersClient\Dto\CustomerStatusResponse**](../Model/CustomerStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceCustomerStatuses

> \Ensi\CustomersClient\Dto\CustomerStatusResponse replaceCustomerStatuses($id, $customer_status_for_create_or_replace)

Замена объекта типа Status

Замена объекта типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$customer_status_for_create_or_replace = new \Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace(); // \Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace | 

try {
    $result = $apiInstance->replaceCustomerStatuses($id, $customer_status_for_create_or_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->replaceCustomerStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **customer_status_for_create_or_replace** | [**\Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace**](../Model/CustomerStatusForCreateOrReplace.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerStatusResponse**](../Model/CustomerStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomerStatuses

> \Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse searchCustomerStatuses($search_customer_statuses_request)

Поиск объектов типа Status

Поиск объектов типа Status

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\StatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customer_statuses_request = new \Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest(); // \Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest | 

try {
    $result = $apiInstance->searchCustomerStatuses($search_customer_statuses_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatusesApi->searchCustomerStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customer_statuses_request** | [**\Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest**](../Model/SearchCustomerStatusesRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse**](../Model/SearchCustomerStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

