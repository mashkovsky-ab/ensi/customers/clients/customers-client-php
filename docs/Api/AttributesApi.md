# Ensi\CustomersClient\AttributesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomerAttributes**](AttributesApi.md#createCustomerAttributes) | **POST** /customers/attributes | Создание объекта типа Attribute
[**deleteCustomerAttribute**](AttributesApi.md#deleteCustomerAttribute) | **DELETE** /customers/attributes/{id} | Удаление объекта типа Attribute
[**getCustomerAttribute**](AttributesApi.md#getCustomerAttribute) | **GET** /customers/attributes/{id} | Получение объекта типа Attribute
[**replaceCustomerAttribute**](AttributesApi.md#replaceCustomerAttribute) | **PATCH** /customers/attributes/{id} | Замена объекта типа Attribute
[**searchCustomerAttributes**](AttributesApi.md#searchCustomerAttributes) | **POST** /customers/attributes:search | Поиск объектов типа Attribute



## createCustomerAttributes

> \Ensi\CustomersClient\Dto\CustomerAttributeResponse createCustomerAttributes($customer_attribute_for_create_or_replace)

Создание объекта типа Attribute

Создание объекта типа Attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AttributesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$customer_attribute_for_create_or_replace = new \Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace(); // \Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace | 

try {
    $result = $apiInstance->createCustomerAttributes($customer_attribute_for_create_or_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributesApi->createCustomerAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_attribute_for_create_or_replace** | [**\Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace**](../Model/CustomerAttributeForCreateOrReplace.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAttributeResponse**](../Model/CustomerAttributeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerAttribute

> \Ensi\CustomersClient\Dto\EmptyDataResponse deleteCustomerAttribute($id)

Удаление объекта типа Attribute

Удаление объекта типа Attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AttributesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCustomerAttribute($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributesApi->deleteCustomerAttribute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CustomersClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCustomerAttribute

> \Ensi\CustomersClient\Dto\CustomerAttributeResponse getCustomerAttribute($id, $include)

Получение объекта типа Attribute

Получение объекта типа Attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AttributesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCustomerAttribute($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributesApi->getCustomerAttribute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAttributeResponse**](../Model/CustomerAttributeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceCustomerAttribute

> \Ensi\CustomersClient\Dto\CustomerAttributeResponse replaceCustomerAttribute($id, $customer_attribute_for_create_or_replace)

Замена объекта типа Attribute

Замена объекта типа Attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AttributesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$customer_attribute_for_create_or_replace = new \Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace(); // \Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace | 

try {
    $result = $apiInstance->replaceCustomerAttribute($id, $customer_attribute_for_create_or_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributesApi->replaceCustomerAttribute: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **customer_attribute_for_create_or_replace** | [**\Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace**](../Model/CustomerAttributeForCreateOrReplace.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\CustomerAttributeResponse**](../Model/CustomerAttributeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCustomerAttributes

> \Ensi\CustomersClient\Dto\SearchCustomerAttributesResponse searchCustomerAttributes($search_customer_attributes_request)

Поиск объектов типа Attribute

Поиск объектов типа Attribute

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomersClient\Api\AttributesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_customer_attributes_request = new \Ensi\CustomersClient\Dto\SearchCustomerAttributesRequest(); // \Ensi\CustomersClient\Dto\SearchCustomerAttributesRequest | 

try {
    $result = $apiInstance->searchCustomerAttributes($search_customer_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributesApi->searchCustomerAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_customer_attributes_request** | [**\Ensi\CustomersClient\Dto\SearchCustomerAttributesRequest**](../Model/SearchCustomerAttributesRequest.md)|  |

### Return type

[**\Ensi\CustomersClient\Dto\SearchCustomerAttributesResponse**](../Model/SearchCustomerAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

