<?php

namespace Ensi\CustomersClient;

class CustomersClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CustomersClient\Api\StatusesApi',
        '\Ensi\CustomersClient\Api\AddressesApi',
        '\Ensi\CustomersClient\Api\CustomersApi',
        '\Ensi\CustomersClient\Api\AttributesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CustomersClient\Dto\CustomerAttributeFillableProperties',
        '\Ensi\CustomersClient\Dto\CustomerAddressForCreate',
        '\Ensi\CustomersClient\Dto\CustomerAddressReadonlyProperties',
        '\Ensi\CustomersClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CustomersClient\Dto\CustomerStatusReadonlyProperties',
        '\Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse',
        '\Ensi\CustomersClient\Dto\CustomerAttributes',
        '\Ensi\CustomersClient\Dto\PaginationTypeEnum',
        '\Ensi\CustomersClient\Dto\CustomerStatusFillableProperties',
        '\Ensi\CustomersClient\Dto\CustomerIncludes',
        '\Ensi\CustomersClient\Dto\CustomerAttributeForCreateOrReplace',
        '\Ensi\CustomersClient\Dto\CustomerAddressResponse',
        '\Ensi\CustomersClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CustomersClient\Dto\MessageForChangeEmail',
        '\Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse',
        '\Ensi\CustomersClient\Dto\SearchCustomersFilter',
        '\Ensi\CustomersClient\Dto\CustomerForReplace',
        '\Ensi\CustomersClient\Dto\CustomerResponse',
        '\Ensi\CustomersClient\Dto\ErrorResponse',
        '\Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest',
        '\Ensi\CustomersClient\Dto\VerifyEmailRequest',
        '\Ensi\CustomersClient\Dto\CustomerForCreate',
        '\Ensi\CustomersClient\Dto\CustomerAddress',
        '\Ensi\CustomersClient\Dto\ResponseBodyPagination',
        '\Ensi\CustomersClient\Dto\CustomerStatusResponse',
        '\Ensi\CustomersClient\Dto\SearchCustomerAttributesResponse',
        '\Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest',
        '\Ensi\CustomersClient\Dto\CustomerGenderEnum',
        '\Ensi\CustomersClient\Dto\ModelInterface',
        '\Ensi\CustomersClient\Dto\SearchCustomersResponseMeta',
        '\Ensi\CustomersClient\Dto\EmptyDataResponse',
        '\Ensi\CustomersClient\Dto\RequestBodyPagination',
        '\Ensi\CustomersClient\Dto\SearchCustomersRequest',
        '\Ensi\CustomersClient\Dto\CustomerAddressForReplace',
        '\Ensi\CustomersClient\Dto\Customer',
        '\Ensi\CustomersClient\Dto\CustomerAddressFillablePropertiesAddress',
        '\Ensi\CustomersClient\Dto\SearchCustomerAttributesRequest',
        '\Ensi\CustomersClient\Dto\CustomerAddressFillableProperties',
        '\Ensi\CustomersClient\Dto\CustomerAttributeResponse',
        '\Ensi\CustomersClient\Dto\SearchCustomersResponse',
        '\Ensi\CustomersClient\Dto\CustomerFillableProperties',
        '\Ensi\CustomersClient\Dto\CustomerStatuses',
        '\Ensi\CustomersClient\Dto\CustomerReadonlyProperties',
        '\Ensi\CustomersClient\Dto\CustomerStatusForCreateOrReplace',
        '\Ensi\CustomersClient\Dto\CustomerAttributeReadonlyProperties',
        '\Ensi\CustomersClient\Dto\MultipartFileUploadRequest',
        '\Ensi\CustomersClient\Dto\File',
        '\Ensi\CustomersClient\Dto\Error',
        '\Ensi\CustomersClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CustomersClient\Dto\ResponseBodyCursorPagination',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CustomersClient\Configuration';
}
