<?php
/**
 * AddressesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CustomersClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Customers
 *
 * Управление клиентами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\CustomersClient;

use \Ensi\CustomersClient\Configuration;
use \Ensi\CustomersClient\ApiException;
use \Ensi\CustomersClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * AddressesApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\CustomersClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class AddressesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createCustomerAddress
     *
     * Создание объекта типа CustomerAddress.
     *
     */
    public function testCreateCustomerAddress()
    {
    }

    /**
     * Test case for deleteCustomerAddress
     *
     * Удаление объекта типа CustomerAddress.
     *
     */
    public function testDeleteCustomerAddress()
    {
    }

    /**
     * Test case for getCustomerAddress
     *
     * Получение объекта типа CustomerAddress.
     *
     */
    public function testGetCustomerAddress()
    {
    }

    /**
     * Test case for replaceCustomerAddress
     *
     * Замена объекта типа CustomerAddress.
     *
     */
    public function testReplaceCustomerAddress()
    {
    }

    /**
     * Test case for searchCustomerAddresses
     *
     * Поиск объектов типа Customer Address.
     *
     */
    public function testSearchCustomerAddresses()
    {
    }

    /**
     * Test case for setCustomerAddressesAsDefault
     *
     * Устанавливает Customer Address как адрес по-умолчанию.
     *
     */
    public function testSetCustomerAddressesAsDefault()
    {
    }
}
